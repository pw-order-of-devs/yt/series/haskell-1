### As-Pattern

* Named patterns to be used on right side of expression
* Always successfully matched - however sub-pattern might fail

```f (a:ab) = a:a:ab``` - duplicates first element \
```f s@(a:ab) = a:s``` - same, simplified

### Wild-cards

* Matching against a value that doesn't really matter

```head (a:_)  = a``` - take first element, discard the others \
```tail (_:ab) = ab``` - take last element, discard the others


### Case expressions

* More primitive than pattern matching
* Not necessarily a function
  ```if cond1 then res1 else if cond2 then res2 else res3```
```
case cond of True  -> res1
             False -> res2
```

### Pattern matching

* Testing an expression for presence of given pattern
* Can result in either success, failure or divergence (error)
* The process goes top-down, left-right
* Error (_|_) - runtime error - if all expressions fail
* Right-hand types must match for each expression 
* Each definition is a function (trade-off?)
```
-- guarded equations
cmp  x y |  x >  y  =   1
         |  x == y  =   0
         |  x <  y  =  -1
```
```
fibI n = case n of
  0 -> 0
  1 -> 1
  _ -> fibI (n - 1) + fibI (n - 2)
```
```
fibI n | n elem [0, 1] = n
fibI n | n >= 2 = fibI (n - 1) + fibI (n - 2)
```
```
fibI 0 = 0
fibI 1 = 1
fibI n | n >= 2 = fibI (n - 1) + fibI (n - 2)
```

### Lazy patterns

* Takes form of ```~pattern``` or ```let (a,b) = pattern```
* ```let``` matches the top-most constructor only
* Always match without checking the matched value
```
f1 :: Either e Int -> Int
f1 ~(Right 1) = 42 -- any Left will pass as 42
```
```
f1 :: Either e Int -> Int
f1 ~(Right x) = x + 1 -- non-numeric Left will result in error
```

### Lexical Scoping, Nested forms

* Used to create a nested scope within an expression - for local bindings
* only allowed for signatures, function bindings, and pattern bindings.
* ```let``` - useful for nested set of bindings
```
f x y = let fa = (x*2 + y*2)
            fb = (x/2 + y/2)
          in
            if fa > fb
            then fa
            else fb
```
* ```where``` - scope bindings for guarded equations
* ```where``` is only allowed at the top level of a set of equations or case expression
```
f x y  | y > xx   = ..
       | y == xx  = ..
       | y < xx   = ..
     where xx = x * x
```
