## Haskell

Created: 1990 \
Paradigm: functional \
Stable version: Haskell 2010 \
Compiler: GHC (Glasgow Haskell Compiler) - comparable to GCC

### Features
- Purely functional programming language
  - every function is "pure" - a function in the mathematical sense
  - no statements or instructions - only expressions
  - variables cannot be mutated
- Declarative, statically typed
  - type composition is verified during compilation
  - no need to explicitly define types in program 
- Concurrent
  - concurrency is well-supported
  - efficient parallel garbage collector
  - light-weight
- Lazy
  - function arguments aren't evaluated
  - performance benefits
- Packages
  - open source contribution
  - active community