## Haskell - Stack example

initialize project: \
```stack new helloworld new-template ```

build project: \
```cd helloworld``` \
```stack build```

run project: \
```stack exec helloworld-exe```

clean project: \
```stack clean <package>``` - deletes the local working directories containing compiler output \
```stack purge``` - deletes the local stack working directories

\
files to check out:
- Setup.hs
  - component of CABAL build system
  - not needed, but considered as a good practice to include it
- stack.yaml
  - project-level settings
  - ```packages``` - mapping for local packages - for multi-package projects
  - ```resolver``` - definition of *how to build the package*: version of GHC and package dependencies, ...
- package.yaml
  - package format, dependencies, ...