module Menu
    ( matchArg
    ) where

import Lib

matchArg :: [String] -> IO ()
matchArg input = case head input of
  "help" -> printMenu
  "version" -> printVersion
  "fib" -> case compare (length input) 2 of
    EQ -> fib (input!!1)
    _ -> putStrLn "Missing value parameter for fib"
  "prime" -> case compare (length input) 2 of
    EQ -> prime (input!!1)
    _ -> putStrLn "Missing value parameter for prime"
  "gcd" -> case compare (length input) 3 of
    EQ -> mGcd (input!!1) (input!!2)
    _ -> putStrLn "Missing value parameter(s) for gcd"
  "lcm" -> case compare (length input) 3 of
    EQ -> mLcm (input!!1) (input!!2)
    _ -> putStrLn "Missing value parameter(s) for lcm"
  "chain" -> case compare (length input) 1 of
    GT -> chain (drop 1 (take (length input) input))
  _ -> printMenu
