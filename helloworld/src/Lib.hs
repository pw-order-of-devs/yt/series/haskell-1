module Lib
    ( printMenu,
      printVersion,
      fib,
      prime,
      mGcd,
      mLcm,
      chain
    ) where

import Safe (readMay)
import Text.Regex.Posix

printMenu :: IO ()
printMenu = do
  putStrLn "Usage:"
  putStrLn "* help - prints this menu"
  putStrLn "* version - prints app version"
  putStrLn "* fib <num> - calculates nth item in Fibonacci sequence"
  putStrLn "* prime <num> - checks if given number is prime"
  putStrLn "* gcd <num> <num> - greatest common divisor"
  putStrLn "* lcm <num> <num> - least common multiple"
  putStrLn "* chain <str>+ - some chain operation"

printVersion :: IO ()
printVersion = putStrLn "Version: 0.1.0"

isNum :: String -> Bool
isNum i = length (readMay (i =~ "[0-9]+") :: Maybe Int) == 1

fib :: String -> IO ()
fib i = if isNum i
  then putStrLn $ show (fibI (read i :: Int))
  else putStrLn $ "Input error: " ++ i

fibI :: Int -> Int
fibI n = case n of
  0 -> 0
  1 -> 1
  _ -> fibI (n - 1) + fibI (n - 2)

prime :: String -> IO ()
prime i = if isNum i
  then putStrLn $ i ++ (if primeI (read i :: Int) then " is " else " is not ") ++ "prime"
  else putStrLn $ "Input error: " ++ i

primeI :: Int -> Bool
primeI k = if k > 1 then null [ x | x <- [2..k - 1], k `mod` x == 0] else False

mGcd :: String -> String -> IO ()
mGcd a b = if isNum a && isNum b
  then putStrLn $ ("gcd of " ++ a ++ " and " ++ b ++ " is " ++ show (mGcdI (read a :: Int) (read b :: Int)))
  else putStrLn $ ("Input error: " ++ a ++ ", " ++ b)

mGcdI :: Int -> Int -> Int
mGcdI a b =
  if a == 0 then b
  else if b == 0 then a
  else if a == b then a
  else if a > b then mGcdI (a-b) b
  else mGcdI a (b-a)

mLcm :: String -> String -> IO ()
mLcm a b = if isNum a && isNum b
  then putStrLn $ ("lcm of " ++ a ++ " and " ++ b ++ " is " ++ show (mLcmI (read a :: Int) (read b :: Int)))
  else putStrLn $ ("Input error: " ++ a ++ ", " ++ b)

mLcmI :: Int -> Int -> Int
mLcmI a b = (a `div` (mGcdI a b)) * b

chain :: [String] -> IO ()
chain lst = print $ sum [n ^ 2 | n <- [negate m | m <- [length l | l <- lst]]]
--chain lst = print $ sum [n ^ 2 | n <- map negate (map length lst)]
