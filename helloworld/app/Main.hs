module Main where

import System.Environment
import System.Exit as EX

import Menu

main :: IO ()
main = do
  args <- getArgs
  matchArg args
  exitWith EX.ExitSuccess
