### Equality, Order

Usage: to compare variables, sort lists ..

```
data Ordering  =  EQ | LT | GT 
compare        :: Ord a => a -> a -> Ordering
```

Ord a - typeclass constraint indicating function working on type `a` if it is comparable/orderable. 

### Enumeration

Enumerated type of unique values.\
Commonly used to define accepted values/options.

```
data MyEnumeration = Foo | Bar | Baz

instance Enum MyEnumeration 
 toEnum 0 = Foo
 toEnum 1 = Bar
 toEnum 2 = Baz

 fromEnum Foo = 0
 fromEnum Bar = 1
 fromEnum Baz = 2 
 ```
```
data MyEnumeration = Foo | Bar | Baz deriving (Enum)
```
```
[Foo .. Baz] => [Foo, Bar, Baz]
```

### Read, Show

Used to convert from and into String - mostly, IO purposes.

Show - parse value into string\
```show :: Show a => a -> String```\
```
showTree :: (Show a) => Tree a -> String
showTree (Leaf x)       =  show x
showTree (Branch l r)   =  "<" ++ showTree l ++ "|" ++ showTree r ++ ">"
```

Read - parse string into value\
```read :: Read a => String -> a```
```
readTree               :: (Read a) => String -> (Tree a)
readTree ('<':s)       =  [(Branch l r, u) | (l, '|':t) <- readTree s,
                                             (r, '>':u) <- readTree t ]
readTree s             =  [(Leaf x, t)     | (x,t)      <- read s]
```

Such methods could be defined as Show/Read instances - so, show/read function could be used on Tree type:

```
instance Show a => Show (Tree a) where
    show x = showTree x

instance Read a => Read (Tree a) where
    read s = readTree s
```

### Bounded

```
class  Bounded a  where
    minBound, maxBound :: a
```

Used to name upper/lower limits of given type.
May be derived from any enumeration type.

```
boundedFn :: (Bounded a) => a -> BoundCheck a
boundedFn x | minBound <= x && x <= maxBound = BC x
            | otherwise = error "..."
```

### Derived instances

`deriving` clause - implicitly produced `T` instance that can be automatically derived from data

```data  Tree a            =  Leaf a | Branch (Tree a) (Tree a)  deriving Eq```

```
instance  (Eq a) => Eq (Tree a)  where
    (Leaf x)     == (Leaf y)        =  x == y
    (Branch l r) == (Branch l' r')  =  l == l' && r == r'
    _            == _               =  False
```
```
instance  (Ord a) => Ord (Tree a)  where
    (Leaf _)     <= (Branch _)      =  True
    (Leaf x)     <= (Leaf y)        =  x <= y
    (Branch _)   <= (Leaf _)        =  False
    (Branch l r) <= (Branch l' r')  =  l == l' && r <= r' || l <= l'
```

Eq and Ord are almost always derived instead of implemented by user.\
Own definitions should be delivered only in some particular, non-standard cases.

Another example - enum (mentioned above):

```
data Day =
      Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday
      deriving (Enum)
```
```
[Wednesday .. Friday]	=>	[Wednesday, Thursday, Friday]
[Monday, Wednesday ..]	=>	[Monday, Wednesday, Friday, Sunday]
```

Types composed from ones having Read/Show instances also have Read/Show instances.

```
show [Monday .. Wednesday] => "[Monday,Tuesday,Wednesday]"
```