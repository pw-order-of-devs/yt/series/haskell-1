## Haskell - how to install

- GHC: Glasgow Haskell Compiler
- CABAL: Common Architecture for Building Applications and Libraries
  - building/packaging tool
  - released in 2005
  - simplification for packaging of Haskell modules
  - CABAL hell problem - breaking dependencies caused by version conflicts
- Stack
  - tool for building projects and managing dependencies
  - uses curated version of CABAL library
  - released in 2015 to solve some CABAL problems

### Platforms:

- Linux:
  - distro choice: https://www.haskell.org/platform/linux.html
  - generic:
    - ghc + cabal: \
      ```curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh ```
    - stack: \
      ```curl -sSL https://get.haskellstack.org/ | sh```
- MacOS: https://www.haskell.org/platform/mac.html
- Windows: https://www.haskell.org/platform/windows.html