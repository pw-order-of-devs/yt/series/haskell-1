module Main where

import Conn (initConn, mainLoop)

main :: IO ()
main = initConn >>= mainLoop
