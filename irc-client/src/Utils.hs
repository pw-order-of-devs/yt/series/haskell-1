module Utils
    ( write
    ) where

import System.IO

write :: Handle -> String -> String -> IO ()
write conn cmd args = writeToServer conn cmd args >> writeToConsole conn cmd args

writeToServer :: Handle -> String -> String -> IO ()
writeToServer conn cmd args = hPutStr conn (buildMsg cmd args)

writeToConsole :: Handle -> String -> String -> IO ()
writeToConsole conn cmd args = putStr ("> " ++ (buildMsg cmd args))

buildMsg :: String -> String -> String
buildMsg cmd args = cmd ++ " " ++ args ++ "\r\n"
