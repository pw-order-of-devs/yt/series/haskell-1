module Conn
    ( initConn,
      mainLoop
    ) where

import System.IO
import qualified Network.Socket as NS
import qualified Control.Concurrent as CC
import qualified Control.Monad as CM

import Utils

serverAddr = "irc.freenode.org" :: String
port = 6667 :: NS.PortNumber
channel = "#irc-test-channel"
nickname = "irc-test-nickname"

initConn :: IO Handle
initConn = connectTo serverAddr port >>= writeConnData

connectTo :: NS.HostName -> NS.PortNumber -> IO Handle
connectTo host port = do
  addr : _ <- NS.getAddrInfo Nothing (Just host) (Just (show port))
  sock <- NS.socket (NS.addrFamily addr) (NS.addrSocketType addr) (NS.addrProtocol addr)
  NS.connect sock (NS.addrAddress addr)
  NS.socketToHandle sock ReadWriteMode

writeConnData :: Handle -> IO Handle
writeConnData conn =
  write conn "NICK" nickname >>
  write conn "USER" (nickname ++ " 0 * :irc-test") >>
  write conn "JOIN" channel >>
  return conn

mainLoop :: Handle -> IO ()
mainLoop conn = reqLoop conn >> resLoop conn

reqLoop :: Handle -> IO CC.ThreadId
reqLoop conn = CC.forkIO $ CM.forever $ getLine >>= write conn ""

resLoop :: Handle -> IO ()
resLoop conn = CM.forever $ (hGetLine conn) >>= print
